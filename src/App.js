import "./App.css";
import Dashboard from "./Templates/Dashboard";
import Footer from "./Templates/Footer";
import Header from "./Templates/Header";
import Menu from "./Templates/Menu";

function App() {
  return (
    <div className="wrapper">
      <Header />
      <Menu />
      <Dashboard />
      <Footer />
    </div>
  );
}

export default App;
